# Facebook için Etik Alternatifler

## Friendica

Friendica, etkinlikler ve fotoğraf albümleri gibi özelliklere sahip Facebook tarzı bir sosyal ağdır.

[Mastodon](../twitter) gibi, Friendica da [federe]() yapıyla birbirine bağlanmış birçok küçük siteden oluşur. Friendica ağını oluşturan bu küçük sitelere düğüm denir.

Friendica, Mastodon, [PixelFed](../instagram), [PeerTube](../youtube) ve diğer pek çok alternatif sosyal ağla birleşik yapıdadır; böylece bu ağlardaki kişilerle arkadaş olabilirsiniz.

Kaydolmaya karar verirseniz, aşağıdaki sitelere kayıt olabilirsiniz. Ayrıca alternatif sosyal medyayı kullanmaya nasıl başlayacağınızla ilgili [ipuçlarımızı](../alternatif-sosyal-medyaya-baslangic) da okumak isteyebilirsiniz. Ayrıca YARIMADA'da yayımlanan [Facebook’ta Olmak ya da Olmamak](http://yarimada.gen.tr/?p=328) başlıklı yazıyı veya Ekşi Sözlük'teki [friendica](https://eksisozluk.com/friendica--4107032) başlığında yazılanları da okuyabilirsiniz.

**İNTERNET SİTESİ** - [Friendica](https://friendi.ca/)

**NEREDEN KAYIT OLUNUR** - [Nerdica](https://nerdica.net/), [libranet.de](https://libranet.de/), [iSurf Social](https://social.isurf.ca/) ve daha birçok başka yerden

**ANDROID UYGULAMASI** - [Fedilab](https://play.google.com/store/apps/details?id=app.fedilab.android)

[ana sayfaya dön](../..)
