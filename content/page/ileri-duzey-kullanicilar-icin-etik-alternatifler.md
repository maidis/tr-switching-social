# İleri Düzey Kullanıcılar için Etik Alternatifler

**Sosyal Değişim**, teknik olmayan insanlar içindir ve daima böyle kalacak.

Ancak, teknolojiyi çok kullanıyor ve bilgisayarlar hakkında epey iyi bir bilgi sahibiyseniz aşağıdaki siteleri incelemek isteyebilirsiniz. Bu siteler de etik ve gizlilik dostu alternatifler sunar, sadece buradaki bilgilerden yararlanacak kişilerin daha çok ön bilgi sahibi olduğu varsayılır:

[De-google-ify Internet](https://degooglisons-internet.org/en/alternatives) - Özgür yazılım hayranı Framasoft'un web sitesi sahipleri, iş insanları, geliştiriciler ve diğer insanlar için sunduğu birçok farklı alanda mahremiyet dostu uygulamaların ve internet hizmetlerinin yer aldığı kapsamlı bir liste.

[DarkPatterns.org](https://www.darkpatterns.org/) - Web sitelerinin sizi isteğiniz dışında şeyler yaptırmaya kandırmak için kullandıkları tüm kirli numaralara büyüleyici bir bakış.

[Prism Break](https://prism-break.org/tr/) -  İleri düzey kullanıcılar için toplu gözetim programlarından nasıl kaçınılacağı konusunda bir rehber.

[LineageOS](https://lineageos.org/) - Google'ın uygulamalarının ve hizmetlerinin olmadığı, Android akıllı telefonlar için alternatif bir özgür işletim sistemi. LineageOS uyumlu telefonların listesi [şurada](https://download.lineageos.org/) görülebilir.

[instances.social](https://instances.social/) - Gelişmiş kullanıcıların uygun bir Mastodon sitesi seçmesine yardımcı olan bir rehber.

[Fediverse.party](https://fediverse.party/) - Halihazırda mevcut veya geliştirilmekte olan her bir federe sosyal ağın teknik detaylarını içeren, üç boyutlu, hayal oyunu temalı bir rehber.

[The Federation](https://the-federation.info/) -  Tüm federe sosyal ağlar hakkında genel istatistikler (istatistiklerin yalnızca bazı sitelerden geldiğini, bu nedenle tüm sitelerin listelenmediğini unutmayın).

[Compute Freely](https://computefreely.org/) - Bilgisayarınızda Linux'a geçiş için bir rehber.

[Linux Games](https://gitlab.com/Ashpex/Linux_Games) - Linux ve diğer platformlar için özgür oyunların listesi.

[linux oyunları](https://eksisozluk.com/linux-oyunlari--702662) - Bir zamanlar Linux'ta da oyunlar olduğunu göstermek için başlatılmış, ancak artık Linux oyunları pek nadir olmadığı için eskisi kadar aktif şekilde güncellenmeyen bir liste.

[Edebiyatta DRM'siz Yaşam Rehberi](https://anilozbek.blogspot.com/2012/08/edebiyatta-drmsiz-yasam-rehberi.html) - E-kitap okurken tercih edilebilecek uygun biçimler, siteler ve DRM hakkında bir rehber.

[Gayrimerkezi bir dünyanın arifesinde iki yeni adım: GNU MediaGoblin ve GNU Taler](https://ozcanoguz.com.tr/posts/bildiri/) - Merkezi olmayan yapıların ve bunların bir nebze geri kaldığı medya yayın platformları ve ödeme altyapısı noktaları için GNU projesine dahil olan MediaGoblin ve Taler yazılımlarının hedefleri, bugünü ve yarını ele alan bir makale.

[Özgür Lisanslar](http://ozgurlisanslar.org.tr) - Özgür yazılım lisanslarının ve özgür yazılım felsefesinin ülkemizde daha iyi anlaşılmasını sağlamak için Linux Kullanıcıları Derneği tarafından hazırlatılan bir site.

[Ethical Alternatives & Resources](https://ethical.net/resources) - ethical.net'in etik yaşam için hazırladığı bir liste. Listede kitaplardan tarayıcı uzantılarına, TED konuşmalarından uygulamalara kadar birçok alanda öneriler yer alıyor.

[AlternativeTo](https://alternativeto.net/) - Masaüstü yazılımlarına, web uygulamalarına ve mobil uygulamalara alternatifler listeleyen bir site. Alternatifin özgür yazılım olup olmadığı ve hangi platformları desteklediği de söylenmektedir.

[ana sayfaya dön](../..)
