# Sıkça Sorulan Sorular

## Bir alternatif önerebilir miyim?

Evet, tabii ki. [Hakkında](../about/) bölümünde bulabileceğiniz kanallardan bizimle iletişime geçebilirsiniz.

## **Sosyal Değişim**'in içeriğini kendi sitemde alıntılayabilir miyim? **Sosyal Değişim**'i başka dillere çevirebilir miyim?

Evet, **Sosyal Değişim**, kaynak aldığı [switching.social](https://switching.social) gibi [Creative Commons Attribution Sharealike 4.0 Lisansı](https://creativecommons.org/licenses/by-sa/4.0/deed.tr) ile korunmaktadır. Bu sitenin içeriğini kullanırsanız **Sosyal Değişim**'e ve bu lisansa bir bağlantı vermeyi lütfen unutmayın.

**Sosyal Değişim**'in [İngilizce](https://switching.social/), [Fransızca](https://switching.geber.ga/), [Almanca](https://switchingsocial.de/) ve [İtalyanca](https://switching.ml/) dillerinde benzerleri halihazırda bulunmaktadır. Çeviri yapmak istediğiniz dil bunlardan biriyse bu projelerin yürütücüleriyle iletişime geçmeyi düşünebilirsiniz.

## Sitenize destek olabileceğim bir Patreon, Liberapay veya herhangi başka bir bağış hizmeti bulunmakta mıdır?

İlgili hesaplar hazırlandığında burada paylaşılacaktır. Ayrıca **Sosyal Değişim**'e destek olmak istiyorsanız insanlara **Sosyal Değişim**'den bahsedebilir ve **Sosyal Değişim**'in bilinirliğini artırabilirsiniz.

Bunların dışında ne yapacağınız hakkında fikirlerinizin tükendiği kadar çok paranız varsa **Sosyal Değişim**'de bahsedilen ve sizin de kullandığınız mahremiyet dostu projelere bağışta bulunabilir veya bu projelerin ücretli hesaplarına kayıt olabilirsiniz.

## Mahremiyet Bilincini ve Etiği nasıl tanımlarsınız?

Bu sitenin ana fikri, daha iyi mahremiyete sahip, ancak, kullanımı yerini aldıkları siteler kadar kolay olan alternatifler sunmaktır. Bunlar bu sitede mahremiyet bilinçli siteler olarak anılar.

Genellikle daha yüksek düzeyde mahremiyet sağlayan, ancak çoğu kişinin sahip olmadığı seviyede teknik bilgi gerektiren başka alternatifler vardır. Bunlar, sitenin ana bölümünde listelenmemiş olsa da, bir kısmını [İleri Düzey Kullanıcılar için Etik Alternatifler](../ileri-duzey-kullanicilar-icin-etik-alternatifler/) ve [Taslak Alternatifler](../taslak-alternatifler/) bölümlerinde bulabilirsiniz.

Etik kelimesi Türkçe'de birçok farklı şekilde kullanılmaktadır. Bu sitede, kullanıcıların mahremiyetine daha az zarar veren hizmetler ve uygulamalar kastedilmektedir, çünkü bu site kullanıcı mahremiyetini ahlaki açıdan iyi görmektedir.

Listelenen alternatiflerin hiçbiri mükemmel olmasa da çok daha iyi seçeneklerdir.

## Buradaki alternatifler sonunda büyük şirketler tarafından satın alınmayacak mı?

Çoğu durumda hayır, böyle bir şey olamaz.

Burada listelenen alternatiflerin çoğu, satın alınamayacak veya satılamayacak şekilde teknolojik ve yasal olarak yapılandırılmıştır.

Listelenen sosyal ağlar [federedir](), yani satın alınacak tek bir site yok, birbirine bağlı binlerce bağımsız site var. Belirli bir site satılsa bile, kullanıcıları aynı ağdaki başka bir bağımsız siteye geçebilir. Ağın bir bütün olarak kontrolünü ele geçirmek herkes için neredeyse imkansızdır.

Tüm bunların ötesinde, kullanılan yazılımlar, yasal olarak satın alınmasını imkansız kılan kalıcı özgür lisanslar altında yayımlanmaktadır.

## Tarayıcı alternatiflerinde neden Brave listelenmemiştir?

Brave, risk sermayesi tarafından finanse edilen ticari bir şirkettir ve iş modeli reklamcılık etrafında inşa edilmiştir. Brave, göz attığınız sitelerdeki reklamları engeller ve kendi reklamlarını gösterir. Risk sermayesi parasını çevrimiçi reklam tabanlı iş modelleriyle birleştiren diğer şirketler, insanların mahremiyetini er ya da geç ihlal etmiştir.

Ayrıca, Peter Thiel (Palantir'in başkanı ve Facebook'un yönetim kurulu üyesi) Brave'de büyük bir yatırımcıdır.

Tüm bu nedenlerle Brave'i kullanmanızı önermeyiz.

## Maps.me'den neden Google Haritalar alternatiflerinde bahsedilmemiştir?

Maps.me sitesi, 2014 yılında Rus teknoloji devi Mail.Ru tarafından satın alınmıştır ve bugünlerde mahremiyete saygı duymayan izleyiciler ve reklamlar içeren bir uygulamayı öne çıkarmaktadır.

## Messenger alternatiflerinde neden Matrix/Riot yok?

Matrix/Riot, [Slack alternatifleri](../slack/) bölümünde listelenmiştir.

[ana sayfaya dön](../..)
