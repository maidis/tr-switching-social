# Twitter için Etik Alternatifler

## Mastodon

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts" src="https://video.deadsuperhero.com/videos/embed/adffa7af-6063-4e09-8c1c-e8252167315d" frameborder="0" allowfullscreen></iframe>

Mastodon, milyonlarca kullanıcısı olan en popüler etik sosyal ağdır. Sitenin nasıl çalıştığını yukarıdaki videodan izleyebilirsiniz.

Mastodon, kullanıcıların 500 karaktere kadar yazıyı, fotoğrafı ve video yayınını çeşitli gizlilik seviyeleriyle paylaşabileceği bir Facebook ve Twitter karışımı gibidir.

Perdenin arkasındaki Mastodon, çoğu sosyal ağdan biraz farklıdır: büyük bir site değil, hepsi birbiriyle bağlantılı (ve **örnek** denilen) binlerce küçük bağımsız siteden oluşan bir ağdır. Hangi siteye üye olursanız olun tüm siteler bağlantılı olduğu için herhangi bir sitedeki herhangi biriyle arkadaş olabilirsiniz. Siteler çoğunlukla topluluğa aittir ve kullanıcıların bağışlarıyla desteklenir.

Çok sayıda küçük sitenin bir ağa bağlanması federasyon olarak adlandırılmaktadır. Federasyon kavramı belki tanıdık gelmeyebilir, ancak aslında farkında olmasak bile hepimizin kullandığı bir yöntemdir.

Daha da iyisi Mastodon hesabınız, Instagram alternatif [PixelFed](../instagram), YouTube alternatifi [PeerTube](../youtube), Facebook alternatifi [Friendica](../facebook) ve diğer pek çok federe sosyal ağdaki insanları takip etmenizi sağlıyor. Federe sosyal ağların bu birleşimine de **Federevren** denir.

Mastodon'u aşağıdaki sitelerden birine kaydolarak deneyebilirsiniz ve hesabınızı listelenen Mastodon uygulamalarından birinde kullanabilirsiniz. (Yeni kullanıcılar için bazı [ipuçları ve püf noktaları](../alternatif-sosyal-medyaya-baslangic) da hazırladık.)


**İNTERNET SİTESİ** - [mastodon.social](https://mastodon.social/)

**NEREDEN KAYIT OLUNUR** - [mas.to](https://mas.to/), [mstdn.io](https://mstdn.io/), [social.tchncs.de](https://social.tchncs.de/), [mastodon.host](https://mastodon.host/), [social.chinwag.org](https://social.chinwag.org/) ve [daha birçok başka yerden](http://joinmastodon.org/)

**ANDROID UYGULAMALARI** - [Tusky](https://play.google.com/store/apps/details?id=com.keylesspalace.tusky), [Subway Tooter](https://play.google.com/store/apps/details?id=jp.juggler.subwaytooter), [Fedilab](https://play.google.com/store/apps/details?id=app.fedilab.android)

**iPHONE UYGULAMALARI** - [Toot!](https://itunes.apple.com/app/toot/id1229021451), [Mast](https://itunes.apple.com/app/mast/id1437429129), [Amaroq](https://itunes.apple.com/app/amarok-for-mastodon/id1214116200)

**SAILFISH UYGULAMALARI** - [Tooter](https://openrepos.net/content/dysko/tooter)

**WINDOWS UYGULAMALARI** - [Hyperspace](https://hyperspace.marquiskurt.net/), [Sengi](https://nicolasconstant.github.io/sengi/)

**MAC UYGULAMALARI** - [Mastonaut](https://mastonaut.app/), [Hyperspace](https://hyperspace.marquiskurt.net/), [Sengi](https://nicolasconstant.github.io/sengi/)

**LINUX UYGULAMALARI** - [Hyperspace](https://hyperspace.marquiskurt.net/), [Whalebird](https://whalebird.org/), [Sengi](https://nicolasconstant.github.io/sengi/)

**MASTODON TRUNK** - Mastodon'da takip edecek kişileri bulmak için [buraya tıklayın](https://communitywiki.org/trunk/)

**Not:** Mastodon'u kullanmaktan zevk alıyor ve kendi örneğinizi ağın bir parçası olarak başlatmak istiyorsanız bunu, [masto.host](https://masto.host/) adresinde herhangi bir teknik bilgi olmadan çok kolay bir şekilde yapabilirsiniz.

Mastodon ile ilgili çeşitli ek bilgileri ve ipuçlarını da aşağıdaki yazıları okuyarak öğrenebilirsiniz:

* [Twitter'a Çok Sağlam Bir Rakip Geliyor: Mastodon!](https://www.webtekno.com/twitter-mastodon-h27481.html)
* [Mastodon Nedir? Nasıl Kullanılır?](https://ceaksan.com/tr/mastodon-nedir-nasil-kullanilir/)
* [Mastodon Nedir?](http://devnot.com/2017/mastodon-nedir/)
* [mastodon.social - Ekşi Sözlük](https://eksisozluk.com/mastodon-social--5337000)


[ana sayfaya dön](../..)
