# Instagram'a Etik Alternatifler

## PixelFed

Instagram'a [özgür]() ve [federe]() bir alternatif olan PixelFed, filtreleri, yorumları, beğenileri, paylaşımları vb. içeren, ancak reklam ve kullanıcı izlemesi olmayan, fotoğraf odaklı bir sosyal ağdır. Şurada [bir PixelFed hesabı örneği](https://pixelfed.social/earth) ve şurada da [bir örnek gönderi](https://pixelfed.social/p/earth/22346) görebilirsiniz.

Ağ, birbirleriyle konuşan birçok bağımsız siteden oluşur, böylece herhangi bir PixelFed sitesine üye olabilir ve ağın diğer sitelerindeki kişileri de takip edebilirsiniz. (Büyük veya küçük bir siteye katılmanız önemli değil, hepsi aynı ağın parçasıdır.)

Ayrıca, PixelFed siteleri [ActivityPub]() standardını kullanarak birbirine bağlandığından [Mastodon]() gibi diğer ActivityPub sitelerinden milyonlarca insanı takip edebilirsiniz ve onlar da sizi takip edebilir.

Instagram'dan farklı olarak, PixelFed'i kullanmak için bir uygulamaya ihtiyacınız yoktur, telefonlardaki, bilgisayarlardaki ve tabletlerdeki tarayıcınızda çalışır. (Uygulama kullanmayı tercih eden insanlar için geliştirilmekte olan uygulamalar da vardır.)

**NEREDEN KAYIT OLUNUR** - [pixelfed.de](https://pixelfed.de/), [pxlfd.me](https://pxlfd.me/), [fedi.pictures](https://fedi.pictures/), [pix.tedomum.net](https://pix.tedomum.net/), [pix.diaspodon.fr](https://pix.diaspodon.fr/) ve daha birçok başka yerden

**PROJE SİTESİ** - [Pixelfed.org](https://pixelfed.org/)

[ana sayfaya dön](../..)
