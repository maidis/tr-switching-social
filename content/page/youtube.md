# YouTube için Etik Alternatifler

## PeerTube

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts" src="https://peertube.social/videos/embed/8e6f9d33-995d-47b7-9b3a-c077aadd9bbe" frameborder="0" allowfullscreen></iframe>

PeerTube, YouTube'un [özgür]() ve [federe]() bir alternatifidir. Reklam içermeyen ve sizi takip etmeyen, video odaklı bir sosyal ağdır. Hala çok yeni olması nedeniyle görüntülenecek çok içerik yok, ancak çok hızlı büyüyor.

YouTube'un aksine, PeerTube birbiriyle konuşan birçok bağımsız siteden oluşur. Hangi siteye kaydolduğunuz önemli değildir, hepsi aynı ağın bir parçasıdır. PeerTube videolarını, yukarıdaki örnekte gösterildiği gibi başka bir yere de gömebilirsiniz.

Daha da iyisi PeerTube, [ActivityPub]() standardını kullandığı için PeerTube kanallarını [Mastodon]() ve diğer ActivityPub destekli sosyal ağlarda bulunan milyonlarca insan izleyebilir.

**NEREDEN KAYIT OLUNUR** - [pe.ertu.be](https://pe.ertu.be/), [video.tedomum.net](https://video.tedomum.net/), [share.tube](https://share.tube/) ve daha birçok başka yerden

**ANDROID UYGULAMALARI** - [Fedilab](https://play.google.com/store/apps/details?id=app.fedilab.android) (ayrıca Mastodon ile de çalışıyor), [PeerTube Player](https://play.google.com/store/apps/details?id=net.schueller.peertube)

**PROJE SİTESİ** - [JoinPeerTube.org](https://joinpeertube.org/)

[ana sayfaya dön](../..)
