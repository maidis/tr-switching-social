# Doodle için Etik Alternatifler

## Framadate

Framadate, Fransa'nın en büyük etik yazılım kuruluşu Framasoft tarafından sunulan kullanımı kolay [özgür]() bir Doodle tarzı etkinlik ve anket hizmetidir. Hiçbir maliyeti yoktur, sizi izlemez ve siteye kayıt olmanız gerekmez.

Hizmeti kullanmaktan memnun kalırsanız devam etmesine yardımcı olmak için Framasoft'a bağışta bulunabilirsiniz, ancak bu tamamen isteğe bağlıdır.

**İNTERNET SİTESİ** - [Framadate](https://framadate.org/)

[ana sayfaya dön](../..)
