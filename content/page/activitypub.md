# ActivityPub

İnternetin gerçekten de dünyanın en büyük merkezi olmayan ağı olduğu günleri özlüyor musunuz? Her şeyin bir avuç duvarla çevrili bahçeye kilitlenmeden öncesini? Evet, biz de özlüyoruz.

[ActivityPub](https://activitypub.rocks/)'a katılın. ActivityPub, ActivityStreams 2.0 veri biçimine dayanan merkezi olmayan bir sosyal ağ protokolüdür. ActivityPub, W3C Sosyal Web Çalışma Grubu tarafından yayımlanan resmi bir tavsiye W3C standardıdır.

ActivityPub, içerik oluşturmak, güncellemek ve silmek için bir istemci/sunucu UPA'sı sunmasının yanı sıra bildirimleri iletmek ve içeriklere abone olunmasını sağlamak için de bir federe sunucudan sunucuya UPA'sı sağlar.

## Önemli ActivityPub Uygulamalar

### Federe Sunucular

* **Mastodon** - Bir sosyal ağ yazılımı olan Mastodon, 10 Eylül 2017'de yayımlanan 1.6 sürümüyle ActivityPub'ı kullanmaya başladı. Özel iletiler için önceki OStatus protokolünden daha çok güvenlik sağlaması amaçlanmıştır.

* **Pleroma** - ActivityPub'ı uygulayan bir sosyal ağ yazılımı.

* **Misskey** - ActivityPub'ı uygulayan bir sosyal ağ yazılımı.

* **Hubzilla** - Zot kullanan bir içerik yönetim sistemi platformu, Ekim 2017'de çıkan ikinci sürümüyle bir eklenti yardımıyla ActivityPub desteği ekledi.

* **Nextcloud** - Dosya barındırma için bir federe servis.

* **PeerTube** - Video akışı için bir federe servis.

* **Pixelfed** - Görüntü paylaşımı için bir federe servis.

* **Friendica** - Sosyal ağ yazılımı, 2019.01 sürümüyle ActivityPub'ı kullanmaya başladı.

* **Osada** - Zot ve ActivityPub'ı kullanan bir sosyal ağ yazılımı. Merkezi olmayan ağı olabildiğince birbirine bağlamayı amaçlıyor.

* **Zap** - Zot ve ActivityPub'ı kullanan mahremiyet odaklı bir sosyal ağ yazılımı.

ActivityPub'a hakkında daha çok bilgi için aşağıdaki kaynaklara bakabilirsiniz:

* [ActivityPub - Wikipedia](https://en.wikipedia.org/wiki/ActivityPub)
* [ActivityPub - World Wide Web Consortium](https://www.w3.org/TR/activitypub/)
* [activitypub - ekşi sözlük](https://eksisozluk.com/activitypub--5790535)
* [Awesome ActivityPub](https://github.com/BasixKOR/awesome-activitypub)

