# Patreon için Etik Alternatifler

## Liberapay

Liberapay, hayranlarınızın size her hafta, her ay veya her yıl belirli bir miktar bağış göndermesini sağlayan, kullanımı kolay ve [özgür]() bir bağış platformudur.

Ticari platformlardan farklı olarak, Liberapay özgürdür ve kendisi de kitle fonlamasıyla finanse edilmektedir, bu yüzden bağışlarınıza kesinti uygulamaz.


**İNTERNET SİTESİ** - [Liberapay](https://liberapay.com/)


## Open Collective

Open Collective, bağışların nasıl harcandığı konusunda olabildiğince şeffaf olmak isteyen kitle fonu projeleri ve organizasyonları için özel olarak tasarlanmış gelişmiş bir platformdur.

Projelerin hem bir defalık hem de tekrarlayan bağışları/sponsorlukları kabul etmesini sağlar, aynı zamanda ayrıntılı gider hesapları sunulabilmesine ve proje üyelerinin masrafları kayda geçirmesine ve tartışmasına yardımcı olur.

**İNTERNET SİTESİ** - [Open Collective](https://opencollective.com/)


[ana sayfaya dön](../..)
