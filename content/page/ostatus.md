# OStatus

OStatus, federe mikrobloglar için özgür bir standarttır ve bir web sitesindeki kullanıcıların başka bir web sitesindeki kullanıcılarla durum güncellemesi alışverişi yapmasını sağlar.

Standart, Atom, Activity Streams, WebSub, Somon ve WebFinger gibi özgür protokollerin birlikte nasıl kullanılabileceğini açıklar ve farklı mikroblog sunucu uygulamalarının durum güncellemelerini kullanıcıları arasında neredeyse gerçek zamanlı olarak ileri geri yönlendirmesini sağlar.

OStatus federasyonu ilk önce Status.net ve Identi.ca gibi StatusNet kurulumları arasında mümkündü. Ancak Identi.ca daha sonra pump.io'ya geçti. Haziran 2013 itibariyle, bir dizi diğer mikroblog uygulaması ve içerik yönetimi sistemi standardı gerçeklemek istediklerini açıkladı. Aynı ay, StatusNet ve Free Social'ın GNU social projesiyle birleştirileceği açıklandı.

GNU social'ın ilk resmi sürümünün ardından, StatusNet ve Free Social çalışan bir dizi mikroblog sitesi GNU social'a geçmeye başladı. Ancak GNU Social'ın güç aldığı teknolojideki hayal kırıklıkları, Mastodon, Pleroma ve ActiveAc gibi OStatus kullanan ve GNU Social ile uyumlu olmayı amaçlayan birkaç yeni sunucu paketinin ortaya çıkmasına neden oldu.

Ocak 2012'de, OStatus standardını korumak ve daha da geliştirmek için bir W3C Topluluk Grubu açıldı. Bununla birlikte bu grup, Temmuz 2014'te başlatılan W3C Federe Sosyal Web Çalışma Grubunun çalışmalara başlamasıyla kapatıldı.

Bu yeni çalışma grubu, OStatus'un halefi olarak pump.io'da standartlaştırılmış protokollere dayanan [ActivityPub](../activitypub) adlı yeni bir standart oluşturmaya odaklandı.

## OStatus Kullanan Projeler

OStatus kullanan aktif gelişimdeki projeler:

* Friendica

* GNU social

* Hubzilla

* Mastodon

OStatus hakkında daha çok bilgi almak için:

* [OStatus - Wikipedia](https://en.wikipedia.org/wiki/OStatus)
* [OStatus Community Group](https://www.w3.org/community/ostatus/wiki/Main_Page)
* [OStatus - IndieWeb](https://indieweb.org/OStatus)
* [What is the relationship between OStatus, pump.io and ActivityPub?](https://github.com/w3c/activitypub/issues/228)
