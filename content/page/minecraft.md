# Minecraft için Etik Alternatifler

## Minetest

Minetest, devasa dünyalar, çeşitli oyun kipleri, çevrimiçi ve çevrimdışı oynanış, çok oyunculu sunucular, kullanıcılar tarafından oluşturulan çok sayıda kip ve daha çoğunu içeren [özgür]() bir Minecraft tarzı oyundur.

Windows, macOS, Linux ve Android üzerinde oynanabilir.

**İNTERNET SİTESİ** - [Minetest.net](https://www.minetest.net/)

**WINDOWS / MAC / LINUX UYGULAMASI** - [Minetest](https://www.minetest.net/downloads/)

**ANDROID UYGULAMASI** - [Minetest](https://play.google.com/store/apps/details?id=net.minetest.minetest)

[ana sayfaya dön](../..)
