# Meetup için Etik Alternatifler

MeetUp.com, yararlı bir sitedir, ancak tam manasıyla bir bedeli vardır. MeetUp, sadece basit bir grup sayfası olan şey için organizatörlere çok fazla masraf çıkartır. Bu, mastafları kendisi ödemek veya üyelerine ödetmek zorunda kalan organizatörler için haksızlıktır.

Daha iyi, daha adil alternatifler var ve bunları ne kadar çok kullanırsak o kadar iyi olacaklardır.

## GetTogether

GetTogether, özgür bir etkinlik yönetimi sitesidir. Gruplara takım denir ve site otomatik olarak yakınınızdaki takımları ve olayları arar.

Site hala çok yeni ve etkinliklerin çoğu şu an yazılım geliştirmeyle ilgili, ancak herkes herhangi bir konu için bir takım ve etkinlik oluşturabilir.

**İNTERNET SİTESİ** - [GetTogether.community](https://gettogether.community/)


[ana sayfaya dön](../..)
