---
title: sosyaldegisim.com Üzerinde de Yayındayız
date: 2019-06-21
---

2014'te Namecheap'te bir hesap oluşturmuştum ama bu servisi ilk kullanışım 2019'da [sosyaldegisim.com](http://www.sosyaldegisim.com/) ile gerçekleşti.

Kişisel günlüğümde GitLab Pages üzerinde oluşturulan statik bir sitenin nasıl Namecheap'ten satın alınan özel bir alan adı üzerinde yayımlanabileceğini anlatacağım. Kolay bir iş ama ilk kez yapacaklar için bir belgeyi izlemek işleri kolaylaştırıcı olacaktır.

sosyaldegisim.com üzerinde yayındayız ama her şey tamamlanmadı elbette. Hem içerik açısından hem de sitenin düzgün çalışmasıyla ilgili yapılacak çok şey var. Bugün Twitter, Facebook, WhatsApp ve Gmail alternatifleriyle ilgili yazıları oluşturmayı planlıyorum. sosyaldegisim.com üzerindeki bazı bağlantılar hala GitLab Pages'e gidiyor, bunları da düzelteceğim. Uzun vadede de becerebilirsem https üzerinde nasıl çalışabiliriz ona bakacağım.

Şimdilik **Sosyal Değişim**'den haberler bu kadar. Gelişmelerde görüşmek üzere mahremiyetinize iyi bakın :)