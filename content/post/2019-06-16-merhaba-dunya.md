---
title: Merhaba Daha Özgür Dünya!
date: 2019-06-16
---

Bu **Sosyal Değişim**'deki ilk günlük girdisi. Hemen kısa bir tarihçeyle başlayalım.

[Mastodon](https://mastodon.social/)'da biraz zaman geçirdikten sonra haberdar olduğum [switching.social](https://switching.social/)'ı görür görmez çok beğendim ve Türkçe için de böyle bir çalışma olması gerektiğini düşündüm.

Hem topluluğa haber vermek ve yardım istemek hem de halihazırda benim girişmek üzere olduğum böyle bir çalışma varsa tekrar sıfırdan işe girişmek yerine bu çalışmaya katkı verebilmek için [Twitter'da bir paylaşım yaptım](https://twitter.com/anilozbek/status/1139642126403297282). Şimdilik benzer bir çalışma yok görebildiğim kadarıyla.

Bir iki belge okuduktan sonra GitLab'da [statik bir site projesi](https://gitlab.com/maidis/tr-switching-social) oluşturdum ve yavaşça içerik oluşturmaya başladım. switching.social ile de iletişime geçerek projenin kaynak kodlarını bir kod deposunda yayımlayıp yayımlayamacağını [sordum](https://mastodon.social/@maidis/102275308884964907). Projenin yürütücüsü, geliştirici olmadığı için bir kod deposu kullanmadığını söyledi. Kendisinin switching.social'da çeviri metin kullanmadığını çünkü yazılarda verilen bağlantıların da kullanılan dilde olmasının kullanıcılar açısından önemli olduğunu belirtti.

Hala switching.social'ın bir kod deposuna sahip olmasının iyi olacağını düşünüyorum ama yürütücüsünü buna ikna etmeye çalışmak yerine zamanla bunu kendisinin fark etmesini beklemek daha iyi olacak şimdilik.

Kaynak kodlarını bir proje sayfasında paylaşan switching.social'ın çeviri çalışmaları şöyle:

* [visika/it.switching.social](https://github.com/visika/it.switching.social)
* [mavrigata/gr-switching-social](https://gitlab.com/mavrigata/gr-switching-social)
* [swisode/website](https://gitlab.com/swisode/website)
* [freeatlast/website](https://gitlab.com/freeatlast/website)

switching.social'daki tüm içerikleri çevirmeye, sonrasında da Tinder, Disqus, Linkedin gibi orijinal çalışmada olmayan dünya çapındaki sosyal ağlarla ve Türkiyeye özgü sosyal ağlarla ilgili içerik oluşturmaya çalışacağım. Bunları yaparken veya sonrasında bir alan adı satın alıp projeyi bu alan adında yayımlayacağım. Günlükte veya tüm yazılarda yorum desteğini açıp açmama konusunda henüz kararsızım. Pek gerektiğinden değil ama özgür bir çözümle bunun nasıl yapılabileceğini görmek için bir yorum sistemini deneyebilirim.

Şimdilik Sosyal Değişim'den haberler bu kadar. Sizin söylecekleriniz varsa iletişime geçmekten çekinmeyin.
